from django.views.decorators.csrf import csrf_exempt
from django.template import RequestContext
from django.views.generic import DetailView, View
import json
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from apps.imageboard.models import Board, Post


@csrf_exempt
def get_boards_all(request):
    get_boards = Board.objects.all()

    if request.method == "GET":

        i = 0
        json_data = {}
        for item in get_boards:
            i=i+1
            json_data[i] = {}
            json_data[i]['board_name'] = item.name
            json_data[i]['board_slug'] = item.slug
            json_data[i]['time_create'] = str(item.datetime_create)
            json_data[i]['time_update'] = str(item.datetime_update)

        return HttpResponse(json.dumps(json_data), content_type="application/json")

@csrf_exempt
def get_post(request):
    post = request.GET.get('post')
    print post
    get_posts = Post.objects.filter(id=post)
    print get_posts
    if request.method == "GET":
        json_data = {}
        for item in get_posts:
            json_data['post_id'] = item.id
            json_data['post_name'] = item.name
            json_data['post_comment'] = item.comment
            try:
                json_data['post_file'] = item.image.url
            except ValueError:
                json_data['post_file'] = ""
            json_data['post_media'] = item.media
            json_data['post_create'] = str(item.datetime_create)

        return HttpResponse(json.dumps(json_data), content_type="application/json")
