from django.conf.urls import patterns, include, url
from django.contrib import admin
from apps.imageboard.api import get_boards_all, get_post
from views import BoardView, PostView, HomeView


urlpatterns = patterns('',
    url(r'^(?P<slug>\w+)/?$', BoardView.as_view(), name="board"),
    url(r'^(?P<slug>\w+)/(?P<post_id>\w+)/?$', PostView.as_view(), name="post"),
    url(r'^$', HomeView.as_view(), name="home"),
    )