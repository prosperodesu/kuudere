import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "imageboard.settings_staging")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
