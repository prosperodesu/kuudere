import os
import uuid
from django.db import models
from django.db.models.signals import post_delete
from django.dispatch.dispatcher import receiver

from sorl.thumbnail import ImageField

def upload_to(instance, filename):
    fn, fe = os.path.splitext(filename)

    return '/'.join(['images', str(uuid.uuid1())+fe])

class Board(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255, unique=True)
    datetime_create = models.DateTimeField(auto_now_add=True)
    datetime_update = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

class Post(models.Model):
    board = models.ForeignKey('Board', default=0)
    parent = models.ForeignKey('self', null=True, blank=True, related_name='replies')
    name = models.CharField(max_length=255, blank=True)
    comment = models.TextField(max_length=1000, blank=False)
    image = ImageField(upload_to=upload_to, blank=True)
    file = models.FileField(upload_to=upload_to, blank=True)

    media = models.URLField(max_length=255, blank=True)
    datetime_create = models.DateTimeField(auto_now_add=True)
    datetime_update = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.comment

class Reply(models.Model):
    reply_parent = models.ForeignKey('Post')
    child_id = models.IntegerField(default=0)

class Tag(models.Model):
    name = models.CharField(max_length=150,verbose_name='Tag Name', unique=True)
    post_name = models.ManyToManyField(Post, through='Servship',verbose_name='Post name')

class Servship(models.Model):
    tag = models.ForeignKey(Tag ,verbose_name='Tag Name')
    post_name = models.ForeignKey(Post ,verbose_name='Post name')
    tag_name = models.TextField()

    def __str__(self):
        return self.name or "Anonymous"

class Banner(models.Model):
    image = ImageField(upload_to=upload_to)


@receiver(post_delete, sender=Post)
def post_delete(sender, instance, **kwargs):
    instance.image.delete(False)