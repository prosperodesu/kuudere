from rest_framework import serializers
from apps.imageboard.models import Post

class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ('id',
                  'board',
                  'parent',
                  'name',
                  'comment',
                  'image',
                  'file',
                  'media')