import markdown
REPLY_RE = '/(--)\d*/'
class ReplyExtension(markdown.Extension):
    def extendMarkdown(self, md, md_globals):
        reply_tag = markdown.inlinepatterns.SimpleTagPattern(REPLY_RE, 'reply')

        md.inlinePatterns.add('reply', reply_tag, '>not_strong')

def makeExtension(configs=None):
    return ReplyExtension(configs=configs)