from rest_framework import viewsets, filters, generics
from serializers import PostSerializer
from apps.imageboard.models import Post

class PostViewSet(generics.ListAPIView):
    queryset = Post.objects.all()
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('id', 'parent',)
    serializer_class = PostSerializer
