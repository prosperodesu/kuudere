from django.views.generic import TemplateView
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect
from django.contrib import messages
import re

import random

from models import Board, Post, Banner, Reply
from forms import PostForm

class BaseView(TemplateView):
    def get_context_data(self, **kwargs):
        context = super(BaseView, self).get_context_data(**kwargs)
        context['boards'] = Board.objects.all()
        context['banners'] = random.choice(Banner.objects.all())

        return context

class BoardBaseView(BaseView):
    def get_context_data(self, **kwargs):
        context = super(BoardBaseView, self).get_context_data(**kwargs)
        context['board'] = get_object_or_404(Board, slug=self.kwargs.get('slug', None))
        return context

class HomeView(BaseView):
    template_name = "home.html"

class BoardView(BoardBaseView):
    template_name = "board.html"

    def get_context_data(self, **kwargs):
        context = super(BoardView, self).get_context_data(**kwargs)
        context['posts'] = Post.objects.filter(board_id=context['board'].id, parent = None).order_by('-id')
        return context

    def post(self, request, slug):
        context = self.get_context_data()
        form = PostForm(request.POST, request.FILES)
        if form.is_valid():
            post = form.save()
            post.parent = None
            post.board = context['board']


            post.save()

            post = Post.objects.latest('id');

            messages.success(request, "Success!")
            return redirect('post', context['board'].slug, post.id)
        else:
            messages.error(request, form.errors)
            return HttpResponse(form.errors)
            return redirect('board', context['board'].slug)

class PostView(BoardBaseView):
    template_name = "post.html"

    def get_context_data(self, **kwargs):
        context = super(PostView, self).get_context_data(**kwargs)
        context['replyes'] = Reply.objects.all()
        context['post'] = get_object_or_404(Post, pk=self.kwargs.get('post_id', None))
        return context

    def post(self, request, slug, post_id):
        context = self.get_context_data()
        form = PostForm(request.POST, request.FILES)
        if form.is_valid():
            post = form.save()
            parent = Post.objects.get(pk=request.POST['parent_id'])
            comment = request.POST['comment']
            reply_pattern = r">>\d+"
            reply_compile = re.compile(reply_pattern)
            find_replyes = re.findall(reply_compile, comment)
            if find_replyes:
                for item in find_replyes:
                    get_reply = item.split('>>')
                    try:
                        new_reply = Reply()
                        get_post = Post.objects.get(id=get_reply[1])
                        new_reply.reply_parent = get_post
                        new_reply.child_id = post.id
                        new_reply.save()
                    except IndexError:
                        pass

            new_comment = comment
            post.comment = new_comment
            post.parent = parent
            post.board = context['board']
            post.save()

            messages.success(request, "Success!")
        else:
            messages.error(request, form.errors)
            return HttpResponse(form.errors)

        return redirect('post', slug, post_id)

class AllBoardsApi(BaseView):
    def get_context_data(self, request):

        context = dict()
        get_boards = Post.objects.all()
        if request.method == "GET":
            i = 0
            json_data = {}
            for item in get_boards:
                i=i+1
                json_data[i] = {}
                json_data[i]['board_name'] = item.name
                json_data[i]['board_slug'] = item.slug
                json_data[i]['time_create'] = item.datetime_create
                json_data[i]['time_update'] = item.datetime_update
                return HttpResponse(json_data)