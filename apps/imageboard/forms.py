from django import forms
from django.core import validators
from models import Post

class PostForm(forms.ModelForm):
    parent_id = forms.IntegerField()
    board_id = forms.IntegerField()
    comment = forms.CharField(required=True)

    class Meta:
	    model = Post
	    fields = ['name', 'comment', 'image', 'media']
