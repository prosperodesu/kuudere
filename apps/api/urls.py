from django.conf.urls import include, url, patterns
from django.contrib import admin
from rest_framework import routers
from apps.api.views import PostViewSet

urlpatterns = patterns('apps.api.views',
     url(r'^posts/', PostViewSet.as_view()),
    )
