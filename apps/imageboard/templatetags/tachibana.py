import urlparse
from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()
api_key = "b9edcc53057f4a9a8cc22d7e4d98e1c3"

@register.filter(name='media_filter')
def media_filter(media):
    media_type = ""
    data = urlparse.urlparse(media)
    #query = urlparse.parse_qs(data.query)
    if data.query[0] == "v":
        media_type = "youtube"
    if data.query[0] == "i":
        media_type = "liveleak"

    if media_type == "youtube":
        query = urlparse.parse_qs(data.query)
        video =  query["v"][0]
        return "youtube.com/embed/%s" % video

    if media_type == "liveleak":
        query = urlparse.parse_qs(data.query)
        video =  query["i"][0]
        return "www.liveleak.com/ll_embed?f=%s" % video[1:]

