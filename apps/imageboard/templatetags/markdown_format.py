import markdown
from django import template

register = template.Library()

@register.filter(name='markdown_filter')
def markdown_filter(text):
    return markdown.markdown(text, safe_mode='escape')

