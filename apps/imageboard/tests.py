from django.test import TestCase
from apps.imageboard.models import Board, Post, Tag, Servship
from django.test import Client


class BoardTestCase(TestCase):
    def setUp(self):
        Board.objects.create(name='anime', slug='a')
        Board.objects.create(name='bro', slug='b')
        Board.objects.create(name='prog', slug='pr', )

    def test_get_boards(self):
        client = Client()
        anime = Board.objects.get(name="anime", slug="a")
        bro = Board.objects.get(name="bro", slug="b")
        prog = Board.objects.get(name="prog", slug="pr")

class PostTestCase(TestCase):
    def setUp(self):
        Board.objects.create(name='anime', slug='a')
        Board.objects.create(name='bro', slug='b')
        Board.objects.create(name='prog', slug='pr', )

        anime = Board.objects.get(name='anime', slug='a')
        bro = Board.objects.get(name='bro', slug='b')
        prog = Board.objects.get(name='prog', slug='pr', )

        Post.objects.create(board=anime, name='test_anime', comment='Yarr')
        Post.objects.create(board=bro, name='test_bro', comment='Kate')
        Post.objects.create(board=prog, name='test_pr', comment='Shit')

    def test_get_api_posts(self):
        client = Client()
        i=1
        while i != 4:
            response = client.get('/api?post=' + str(i))
            i=i+1

class TagTestCase(TestCase):
    def setUp(self):
        Board.objects.create(name='anime', slug='a')
        Board.objects.create(name='bro', slug='b')
        Board.objects.create(name='prog', slug='pr')

        anime = Board.objects.get(name='anime', slug='a')
        bro = Board.objects.get(name='bro', slug='b')
        prog = Board.objects.get(name='prog', slug='pr', )

        Post.objects.create(board=anime, name='test_anime', comment='Yarr')
        Post.objects.create(board=bro, name='test_bro', comment='Kate')
        Post.objects.create(board=prog, name='test_pr', comment='Shit')

    def test_get_tags(self):
        test_anime = Post.objects.get(name='test_anime')
        test_bro = Post.objects.get(name='test_bro')
        test_pr = Post.objects.get(name='test_pr')


        new_yoba = Tag.objects.create(name='yoba')
        new_peka = Tag.objects.create(name='peka')
        new_dog = Tag.objects.create(name='dog')
        new_cat = Tag.objects.create(name='cat')
        new_mouse = Tag.objects.create(name='mouse')

        new_servnship = Servship(tag=new_yoba, post_name=test_anime, tag_name='test')

        new_servnship_peka = Servship(tag=new_peka, post_name=test_bro, tag_name='test1')

        new_servnship_dog = Servship(tag=new_dog, post_name=test_pr, tag_name='test2')

        new_servnship_test_cat = Servship(tag=new_cat, post_name=test_bro, tag_name='test3')

        new_servnship_test_mouse = Servship(tag=new_mouse, post_name=test_anime, tag_name='test4')

        new_servnship.save()
        new_servnship_peka.save()
        new_servnship_dog.save()
        new_servnship_test_cat.save()
        new_servnship_test_mouse.save()


